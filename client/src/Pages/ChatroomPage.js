import React from "react";
import { withRouter } from "react-router-dom";
import axios from 'axios'

const ChatroomPage = ({ match, socket }) => {
    const chatroomId = match.params.id;
    const [messages, setMessages] = React.useState([]);
    const messageRef = React.useRef();
    const [userId, setUserId] = React.useState("");
    const [roomName, setRoomName] = React.useState("")

    const getRoomName = () => {
        axios
            .get("http://localhost/chatroom/" + chatroomId, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("ACCESS_TOKEN")
                }
            })
            .then((response) => {
                setRoomName(response.data.name)
            })
            .catch((err) => {

            });
    }

    const sendMessage = () => {
        if (socket) {
            socket.emit("chatroomMessage", {
                chatroomId,
                message: messageRef.current.value,
            });

            messageRef.current.value = "";
        }
    };

    React.useEffect(() => {
        const token = localStorage.getItem("ACCESS_TOKEN");
        if (token) {
            const payload = JSON.parse(atob(token.split(".")[1]));
            setUserId(payload.id);
        }
        if (socket) {
            socket.on("newMessage", (message) => {
                const newMessages = [...messages, message];
                setMessages(newMessages);
            });
        }
        //eslint-disable-next-line
    }, [messages]);

    React.useEffect(() => {
        if (socket) {
            socket.emit("joinRoom", {
                chatroomId,
            });

            getRoomName();
        }

        return () => {
            //Component Unmount
            if (socket) {
                socket.emit("leaveRoom", {
                    chatroomId,
                });
            }
        };
        //eslint-disable-next-line
    }, []);

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            sendMessage()
        }
    }

    return (
        <div className="chatroomPage">
            <div className="chatroomSection">
                <div className="cardHeader">{roomName}</div>
                <div className="chatroomContent">
                    {messages.map((message, i) => (
                        <div key={i} className="message">
                            <span
                                className={
                                    userId === message.userId ? "ownMessage" : "otherMessage"
                                }
                            >
                                {message.name}:
              </span>{" "}
                            {message.message}
                        </div>
                    ))}
                </div>
                <div className="chatroomActions">
                    <div>
                        <input
                            type="text"
                            name="message"
                            placeholder="Say something!"
                            ref={messageRef}
                            onKeyPress={handleKeyPress}
                        />
                    </div>
                    <div>
                        <button className="join" onClick={sendMessage}>
                            Send
            </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withRouter(ChatroomPage);
